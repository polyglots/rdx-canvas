import { ElementRef, Injectable } from '@angular/core';

export interface Snapshot {
  frontLayoutData: string;
  backLayoutData: string;
  problemInfo?: any;
}

export interface CanvasConfig {
  canvas: ElementRef;
  imageSource: string;
  imageLayout: string;
  history: Snapshot[];
  releaseCallback: Function;
}

@Injectable()
export class PaintService {
  private config: CanvasConfig;
  private canvasEl: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;
  private lastX: number;
  private lastY: number;
  private paint: boolean;

  public async initializeCanvas(config: CanvasConfig) {
    this.config = config;
    this.canvasEl = config.canvas.nativeElement;
    this.ctx = this.canvasEl.getContext('2d');
    this.ctx.strokeStyle = 'red';
    this.ctx.lineWidth = 2;
    this.ctx.lineJoin = 'round';
    await this.loadBackgroundImage(config.imageSource);
    this.ctx.save();
    this.addEventListeners();
  }

  public async cleanup() {
    this.ctx.clearRect(0, 0, 700, 600);
    await this.loadBackgroundImage(this.config.imageSource);
    this.removeEventListeners();
  }

  public loadBackgroundImage(source: string) {
    return new Promise(resolve => {
      const outlineImage = new Image();
      outlineImage.src = source;
      outlineImage.crossOrigin = 'Anonymous';
      outlineImage.onload = () => {
        this.ctx.drawImage(outlineImage, 0, 0);
        resolve();
      };
    });
  }

  private addEventListeners() {
    this.canvasEl.addEventListener('mousedown', this.mouseDownHandler);
    this.canvasEl.addEventListener('mousemove', this.mouseMoveHandler);
    this.canvasEl.addEventListener('mouseup', this.releaseHandler);
    this.canvasEl.addEventListener('mouseleave', this.releaseHandler);

    this.canvasEl.addEventListener('touchstart', this.mouseDownHandler);
    this.canvasEl.addEventListener('touchmove', this.mouseMoveHandler);
    this.canvasEl.addEventListener('touchend', this.releaseHandler);
    this.canvasEl.addEventListener('touchcancel', this.releaseHandler);
  }

  private removeEventListeners() {
    if (this.canvasEl) {
      this.canvasEl.removeEventListener('mousedown', this.mouseDownHandler);
      this.canvasEl.removeEventListener('mousemove', this.mouseMoveHandler);
      this.canvasEl.removeEventListener('mouseup', this.releaseHandler);
      this.canvasEl.removeEventListener('mouseleave', this.releaseHandler);

      this.canvasEl.removeEventListener('touchstart', this.mouseDownHandler);
      this.canvasEl.removeEventListener('touchmove', this.mouseMoveHandler);
      this.canvasEl.removeEventListener('touchend', this.releaseHandler);
      this.canvasEl.removeEventListener('touchcancel', this.releaseHandler);
    }
  }

  private mouseDownHandler = e => {
      e.preventDefault();
      this.paint = true;
      const event = (e.touches) ? e.touches[0] : e;
      this.draw(event, false);
  }

  private mouseMoveHandler = e => {
      e.preventDefault();
      if (this.paint) {
          const event = (e.touches) ? e.touches[0] : e;
          this.draw(event, true);
      }
  }

  private draw(event, isDown) {
    const rect = event.target.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    if (isDown) {
      this.ctx.beginPath();
      this.ctx.moveTo(this.lastX, this.lastY);
      this.ctx.lineTo(x, y);
      this.ctx.closePath();
      this.ctx.stroke();
    }
    this.lastX = x;
    this.lastY = y;
  }

  private releaseHandler = event => {
    if (this.paint && (event.type === 'mouseup' || event.type === 'touchend')) {

      const historyLength = this.config.history.length + 1;
      const radius = (historyLength > 9) ? 15 : 10;
      this.ctx.fillStyle = 'rgba(0, 153, 255, 0.8)';
      this.ctx.beginPath();
      this.ctx.arc(this.lastX + (radius - 5), this.lastY + 8, radius, 0, 360);
      this.ctx.fill();

      this.ctx.fillStyle = 'white';
      this.ctx.font = '18px sans-serif';
      this.ctx.textBaseline = 'hanging';
      this.ctx.fillText(`${historyLength}`, this.lastX, this.lastY);

      /* const imageData = this.canvasEl.toDataURL('image/png');
      this.config.history.push({
        layout: this.config.imageLayout,
        imageData
      });
      console.log('History: ', this.config.history); */
    }

    this.paint = false;
    setTimeout(() => {
      this.config.releaseCallback(event);
    }, 100);
  }
}
