import { Injectable, ElementRef } from '@angular/core';
import { async } from 'q';

@Injectable()
export class CanvasService {
  private ctx;
  private lastX;
  private lastY;

  constructor() {}

  public async initializeCanvas(canvas: ElementRef) {
    this.ctx = canvas.nativeElement.getContext('2d');
    this.ctx.strokeStyle = '#000';
    this.ctx.lineWidth = 5;
    this.ctx.lineJoin = 'round';
    await this.loadOutline(this.ctx);
    this.ctx.save();
    return this.ctx;
  }

  public draw(x, y, isDown) {
    if (isDown) {
      this.ctx.beginPath();
      this.ctx.moveTo(this.lastX, this.lastY);
      this.ctx.lineTo(x, y);
      this.ctx.closePath();
      this.ctx.stroke();
    }
    this.lastX = x;
    this.lastY = y;
  }

  public drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }) {
    if (!this.ctx) { return; }

    this.ctx.beginPath();

    if (prevPos) {
      this.ctx.moveTo(prevPos.x, prevPos.y); // from
      this.ctx.lineTo(currentPos.x, currentPos.y);
      this.ctx.stroke();
    }
  }

  public async cleanup() {
    this.ctx.clearRect(0, 0, 700, 600);
    await this.loadOutline(this.ctx);
  }

  private loadOutline(ctx: CanvasRenderingContext2D) {
    return new Promise(resolve => {
      const outlineImage = new Image();
      outlineImage.src = '/assets/outline.jpg';
      outlineImage.onload = () => {
        ctx.drawImage(outlineImage, 0, 0);
        resolve();
      };
    });
  }
}
