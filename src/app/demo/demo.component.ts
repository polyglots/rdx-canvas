import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';
import { PaintService, CanvasConfig, Snapshot } from '../services/paint.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css'],
  providers: [
    { provide: 'bodyDiagramFrontService', useClass: PaintService },
    { provide: 'bodyDiagramBackService', useClass: PaintService },
  ]
})
export class DemoComponent implements OnInit {

  @ViewChild('bodyDiagramFront') bodyDiagramFront: ElementRef;
  @ViewChild('bodyDiagramBack') bodyDiagramBack: ElementRef;

  frontImageURI = '../../assets/images/body_front.jpg';
  backImageURI = '../../assets/images/body_back.jpg';
  redoHistory: Snapshot[] = [];
  undoHistory: Snapshot[] = [];
  bodyProblemData = [];

  constructor(
    @Inject('bodyDiagramFrontService')
    private _bodyDiagramFrontService: PaintService,
    @Inject('bodyDiagramBackService')
    private _bodyDiagramBackService: PaintService,
  ) { }

  async ngOnInit() {
    const frontConfig: CanvasConfig = {
      canvas: this.bodyDiagramFront,
      imageSource: this.frontImageURI,
      imageLayout: 'front',
      history: this.undoHistory,
      releaseCallback: this.onRelease
    };
    await this._bodyDiagramFrontService.initializeCanvas(frontConfig);

    const backConfig: CanvasConfig = {
      canvas: this.bodyDiagramBack,
      imageSource: this.backImageURI,
      imageLayout: 'back',
      history: this.undoHistory,
      releaseCallback: this.onRelease
    };
    await this._bodyDiagramBackService.initializeCanvas(backConfig);
  }

  onRelease = event => {
    if (event.type === 'mouseup' || event.type === 'touchend') {
      this.saveSnapshot();
    }
  }

  undo() {
    const lastEdit: Snapshot = this.undoHistory.pop();

    if (lastEdit) {
      this.redoHistory.push(lastEdit);
      const undoHistoryLength = this.undoHistory.length;
      let frontLayoutData = this.frontImageURI;
      let backLayoutData = this.backImageURI;

      if (undoHistoryLength) {
        const currentEdit = this.undoHistory[undoHistoryLength - 1];
        frontLayoutData = currentEdit.frontLayoutData;
        backLayoutData = currentEdit.backLayoutData;
      }
      this.loadSnapshot({frontLayoutData, backLayoutData});
    }
  }

  redo() {
    const lastEdit: Snapshot = this.redoHistory.pop();
    if (lastEdit) {
      this.undoHistory.push(lastEdit);
      this.loadSnapshot(lastEdit);
    }
  }

  private saveSnapshot() {
    let painDesc = '';
    const frontLayoutData = this.bodyDiagramFront.nativeElement.toDataURL('image/png');
    const backLayoutData = this.bodyDiagramBack.nativeElement.toDataURL('image/png');

    painDesc = window.prompt('Please describe your pain');
    const problemInfo = { id: this.undoHistory.length + 1, desc: painDesc};
    this.undoHistory.push({ frontLayoutData, backLayoutData, problemInfo });
  }

  private loadSnapshot({frontLayoutData, backLayoutData}) {
    this._bodyDiagramFrontService.loadBackgroundImage(frontLayoutData);
      this._bodyDiagramBackService.loadBackgroundImage(backLayoutData);
  }

}
