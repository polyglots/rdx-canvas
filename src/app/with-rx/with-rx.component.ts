import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { from } from 'rxjs/observable/from';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { mergeMap, pairwise, switchMap, takeUntil } from 'rxjs/operators';
import { CanvasService } from '../services/canvas.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-with-rx',
  templateUrl: './with-rx.component.html',
  styleUrls: ['./with-rx.component.css'],
  providers: [CanvasService]
})
export class WithRxComponent implements OnInit, OnDestroy {
  @ViewChild('paintCanvas') paintCanvas: ElementRef;
  canvas: HTMLCanvasElement;
  paint: boolean;
  eventSubscriptions$: Subscription;

  constructor(private canvasService: CanvasService) {}

  async ngOnInit() {
    this.canvas = this.paintCanvas.nativeElement;
    await this.canvasService.initializeCanvas(this.paintCanvas);
    this.createSubscriptions();
  }

  ngOnDestroy() {
    this.eventSubscriptions$.unsubscribe();
  }

  public cleanup() {
    this.canvasService.cleanup();
  }

  private createSubscriptions() {
    const startEvents = ['mousedown', 'touchstart'];
    const drawEvents = ['mousemove', 'touchmove'];
    const endEvents = ['mouseup', 'mouseleave', 'touchend', 'touchcancel'];

    this.eventSubscriptions$ = from(startEvents)
      .pipe(
        mergeMap(startEvent => fromEvent(this.canvas, startEvent)),
        switchMap((startEvent: MouseEvent) => {
          startEvent.preventDefault();
          return from(drawEvents).pipe(
            mergeMap(drawEvent => fromEvent(this.canvas, drawEvent)),
            takeUntil(
              from(endEvents).pipe(
                mergeMap(upEvents => fromEvent(this.canvas, upEvents))
              )
            ),
            pairwise()
          );
        })
      )
      .subscribe((res: [UIEvent, UIEvent]) => this.handleDrawEvents(res));
  }

  private handleDrawEvents(res: [UIEvent, UIEvent]) {
    const [prev, next] = [...res];
    prev.preventDefault();
    next.preventDefault();

    const previousEvent: any = prev instanceof TouchEvent && prev.touches ? prev.touches[0] : prev;
    const currentEvent: any = next instanceof TouchEvent && next.touches ? next.touches[0] : next;
    const prevPos = this.xyPos(previousEvent);
    const currentPos = this.xyPos(currentEvent);
    this.canvasService.drawOnCanvas(prevPos, currentPos);
  }

  private xyPos(event: any) {
    return {
      x: event.clientX - this.canvas.offsetLeft,
      y: event.clientY - this.canvas.offsetTop
    };
  }
}
