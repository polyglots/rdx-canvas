import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WithRxComponent } from './with-rx.component';

describe('WithRxComponent', () => {
  let component: WithRxComponent;
  let fixture: ComponentFixture<WithRxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WithRxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WithRxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
