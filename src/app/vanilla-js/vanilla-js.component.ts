import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CanvasService } from '../services/canvas.service';


@Component({
  selector: 'app-vanilla-js',
  templateUrl: './vanilla-js.component.html',
  styleUrls: ['./vanilla-js.component.css'],
  providers: [CanvasService]
})
export class VanillaJsComponent implements OnInit, OnDestroy {

  @ViewChild('paintCanvas') paintCanvas: ElementRef;
  canvas: HTMLCanvasElement;
  paint: boolean;

  constructor(private canvasService: CanvasService) { }

  async ngOnInit() {
    this.canvas = this.paintCanvas.nativeElement;
    await this.canvasService.initializeCanvas(this.paintCanvas);
    this.addEventListeners();
  }

  ngOnDestroy() {
    this.removeEventListeners();
  }

  public cleanup() {
    this.canvasService.cleanup();
  }

  private addEventListeners() {
    this.canvas.addEventListener('mousedown', this.mouseDownHandler);
    this.canvas.addEventListener('mousemove', this.mouseMoveHandler);
    this.canvas.addEventListener('mouseup', this.releaseHandler);
    this.canvas.addEventListener('mouseleave', this.releaseHandler);

    this.canvas.addEventListener('touchstart', this.mouseDownHandler);
    this.canvas.addEventListener('touchmove', this.mouseMoveHandler);
    this.canvas.addEventListener('touchend', this.releaseHandler);
    this.canvas.addEventListener('touchcancel', this.releaseHandler);
  }

  private removeEventListeners() {
    this.canvas.removeEventListener('mousedown', this.mouseDownHandler);
    this.canvas.removeEventListener('mousemove', this.mouseMoveHandler);
    this.canvas.removeEventListener('mouseup', this.releaseHandler);
    this.canvas.removeEventListener('mouseleave', this.releaseHandler);

    this.canvas.removeEventListener('touchstart', this.mouseDownHandler);
    this.canvas.removeEventListener('touchmove', this.mouseMoveHandler);
    this.canvas.removeEventListener('touchend', this.releaseHandler);
    this.canvas.removeEventListener('touchcancel', this.releaseHandler);
  }

  private mouseDownHandler = e => {
      e.preventDefault();
      this.paint = true;
      const event = (e.touches) ? e.touches[0] : e;
      this.draw(event, false);
  }

  private mouseMoveHandler = e => {
      e.preventDefault();
      if (this.paint) {
          const event = (e.touches) ? e.touches[0] : e;
          this.draw(event, true);
      }
  }

  private draw(event, shouldDraw) {
    this.canvasService.draw(event.pageX - this.canvas.offsetLeft, event.pageY - this.canvas.offsetTop, shouldDraw);
  }

  private releaseHandler = () => this.paint = false;

}
