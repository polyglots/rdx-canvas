import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VanillaJsComponent } from './vanilla-js/vanilla-js.component';
import { WithRxComponent } from './with-rx/with-rx.component';
import { DemoComponent } from './demo/demo.component';

const routes: Routes = [
  { path: 'demo', component: DemoComponent },
  { path: 'vanillajs', component: VanillaJsComponent },
  { path: 'withrx', component: WithRxComponent },
  { path: '', redirectTo: '/demo', pathMatch: 'full' },
  { path: '**', redirectTo: '/demo' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
